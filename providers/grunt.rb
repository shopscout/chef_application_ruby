include Chef::DSL::IncludeRecipe

action :before_compile do
  include_recipe 'nodejs'
end

action :before_deploy do
end

action :before_migrate do
  new_resource.environment['RAILS_ENV'] = new_resource.environment_name

  app_release_path_with_relative = release_path_with_relative

  Chef::Log.info "Running npm install in #{app_release_path_with_relative}"

  directory "#{new_resource.path}/shared/node_modules" do
    owner new_resource.owner
    group new_resource.group
    mode '0755'
  end

  link "#{app_release_path_with_relative}/node_modules" do
    to "#{new_resource.path}/shared/node_modules"
  end

  execute 'npm install' do
    cwd app_release_path_with_relative
    user new_resource.owner
    environment new_resource.environment
    only_if { ::File.exists?(::File.join(app_release_path_with_relative, 'package.json')) }
  end

  if new_resource.bower
    Chef::Log.info "Running bower install in #{app_release_path_with_relative}"

    directory "#{new_resource.path}/shared/bower_components" do
      owner new_resource.owner
      group new_resource.group
      mode '0755'
    end

    bower_components_path = bower_components_path_with_relative
    link "#{bower_components_path}/bower_components" do
      to "#{new_resource.path}/shared/bower_components"
    end

    bower_command = 'node_modules/bower/bin/bower'

    execute 'npm install bower' do
      cwd app_release_path_with_relative
      user new_resource.owner
      environment new_resource.environment
      not_if { ::File.exists?(::File.join(app_release_path_with_relative, bower_command)) }
    end

    execute "#{bower_command} install" do
      cwd app_release_path_with_relative
      user new_resource.owner
      environment new_resource.environment
    end
  end

  if new_resource.grunt_command

    execute 'npm install grunt-cli' do
      cwd app_release_path_with_relative
      user new_resource.owner
      environment new_resource.environment
      not_if { ::File.exists?(::File.join(app_release_path_with_relative, 'node_modules/grunt-cli/bin/grunt')) }
    end

    execute new_resource.grunt_command do
      cwd app_release_path_with_relative
      user new_resource.owner
      environment new_resource.environment
    end
  end
end


action :before_symlink do
end

action :before_restart do
end

action :after_restart do
end

protected

def release_path_with_relative
  release_path_with_relative = new_resource.release_path
  if new_resource.app_relative_path
    release_path_with_relative = ::File.join(new_resource.release_path, new_resource.app_relative_path)
  end
  release_path_with_relative
end

def bower_components_path_with_relative
  release_path_with_relative = new_resource.release_path
  if new_resource.bower_components_relative_path
    release_path_with_relative = ::File.join(new_resource.release_path, new_resource.bower_components_relative_path)
  end
  release_path_with_relative
end