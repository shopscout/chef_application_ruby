#
# Author:: Noah Kantrowitz <noah@opscode.com>
# Cookbook Name:: application_ruby
# Provider:: rails
#
# Copyright:: 2011-2012, Opscode, Inc <legal@opscode.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

action :before_compile do

  if new_resource.bundler.nil?
    new_resource.bundler new_resource.gems.any? { |gem, ver| gem == 'bundler' }
  end

  unless new_resource.migration_command
    command = "rake db:migrate"
    command = "#{bundle_command} exec #{command}" if new_resource.bundler
    new_resource.migration_command command
  end

  new_resource.environment.merge!({
                                      "RAILS_ENV" => new_resource.environment_name,
                                  }) { |k, v1, v2| v1 } # user's environment settings will override

  if new_resource.use_omnibus_ruby
    Chef::Log.warn("Tying your Application to the Chef Omnibus Ruby is not recommended.")
    new_resource.environment.merge!({
                                        "PATH" => [Gem.default_bindir, ENV['PATH']].join(':')
                                    }) { |k, v1, v2| v1 } # user's environment settings will override
  end

  new_resource.symlink_before_migrate.update({
                                                 "database.yml" => "config/database.yml"
                                             })


  if new_resource.symlink_logs
    new_resource.purge_before_symlink.push(relative_log_path)
    new_resource.symlinks.update({relative_log_path => relative_log_path})
  end

end

action :before_deploy do

  new_resource.environment['RAILS_ENV'] = new_resource.environment_name

  install_gems

  create_database_yml

end


action :before_migrate do
  app_release_path_with_relative = release_path_with_relative
  symlink_logs if new_resource.symlink_logs
  new_resource.environment['GIT_SSH'] = "#{new_resource.path}/deploy-ssh-wrapper" if new_resource.deploy_key

  if new_resource.bundler
    Chef::Log.info "Running bundle install"
    directory "#{new_resource.path}/shared/vendor_bundle" do
      owner new_resource.owner
      group new_resource.group
      mode '0755'
    end
    directory "#{app_release_path_with_relative}/vendor" do
      owner new_resource.owner
      group new_resource.group
      mode '0755'
    end
    link "#{app_release_path_with_relative}/vendor/bundle" do
      to "#{new_resource.path}/shared/vendor_bundle"
    end


    common_groups = %w{development test cucumber staging production}
    common_groups -= [new_resource.environment_name]
    common_groups += new_resource.bundler_without_groups
    common_groups = common_groups.join(' ')
    bundler_deployment = new_resource.bundler_deployment
    if bundler_deployment.nil?
      # Check for a Gemfile.lock
      bundler_deployment = ::File.exists?(::File.join(app_release_path_with_relative, "Gemfile.lock"))
    end
    command = "#{bundle_command} install --path=vendor/bundle --without #{common_groups}"
    command += " --deployment" if bundler_deployment
    command += " #{bundle_options}" if bundle_options

    execute command do
      cwd app_release_path_with_relative
      user new_resource.owner
      environment new_resource.environment
    end
  else
    # chef runs before_migrate, then symlink_before_migrate symlinks, then migrations,
    # yet our before_migrate needs database.yml to exist (and must complete before
    # migrations).
    #
    # maybe worth doing run_symlinks_before_migrate before before_migrate callbacks,
    # or an add'l callback.
    execute "(ln -s ../../../shared/database.yml config/database.yml && rake gems:install); rm config/database.yml" do
      cwd app_release_path_with_relative
      user new_resource.owner
      environment new_resource.environment
    end
  end

  gem_names = new_resource.gems.map { |gem, ver| gem }
  if new_resource.migration_command.include?('rake') && !gem_names.include?('rake')
    gem_package "rake" do
      action :install
      not_if "which rake"
    end
  end

  if new_resource.npm
    Chef::Log.info 'Running npm install'
    directory "#{new_resource.path}/shared/node_modules" do
      owner new_resource.owner
      group new_resource.group
      mode '0755'
    end
    link "#{app_release_path_with_relative}/node_modules" do
      to "#{new_resource.path}/shared/node_modules"
    end
    execute 'npm install' do
      cwd app_release_path_with_relative
      user new_resource.owner
      environment new_resource.environment
      only_if { ::File.exists?(::File.join(app_release_path_with_relative, 'package.json')) }
    end
  end
end

action :before_symlink do
  app_release_path_with_relative = release_path_with_relative
  ruby_block "remove_run_migrations" do
    block do
      if node.role?("#{new_resource.name}_run_migrations")
        Chef::Log.info("Migrations were run, removing role[#{new_resource.name}_run_migrations]")
        node.run_list.remove("role[#{new_resource.name}_run_migrations]")
      end
    end
  end

  if new_resource.precompile_assets.nil?
    new_resource.precompile_assets ::File.exists?(::File.join(app_release_path_with_relative, "config", "assets.yml"))
  end

  if new_resource.precompile_assets
    command = "rake assets:precompile"
    command = "#{bundle_command} exec #{command}" if new_resource.bundler
    execute command do
      cwd app_release_path_with_relative
      user new_resource.owner
      environment new_resource.environment
    end
  end

end

action :before_restart do
  ensure_log_directory
end

action :after_restart do
end


protected

def release_path_with_relative
  release_path_with_relative = new_resource.release_path
  if new_resource.app_relative_path
    release_path_with_relative = ::File.join(new_resource.release_path, new_resource.app_relative_path)
  end
  release_path_with_relative
end

def relative_log_path
  log_path = 'log'
  if new_resource.app_relative_path
    log_path = ::File.join(new_resource.app_relative_path, 'log')
  end
  log_path
end

def bundle_options
  new_resource.bundle_options
end

def bundle_command
  new_resource.bundle_command
end

def install_gems
  new_resource.gems.each do |gem, opt|
    if opt.is_a?(Hash)
      ver = opt['version']
      src = opt['source']
    elsif opt.is_a?(String)
      ver = opt
    end
    gem_package gem do
      action :install
      source src if src
      version ver if ver && ver.length > 0
    end
  end
end

def create_database_yml
  if new_resource.database.has_key?("host")
    host = new_resource.database['host']
  else
    host = new_resource.find_database_server(new_resource.database_master_role)
  end
  database_template = new_resource.database_template || 'database.yml.erb'
  cookbook_name = new_resource.database_template ? new_resource.cookbook_name.to_s : 'application_ruby'
  if new_resource.database_template_cookbook_name
    cookbook_name = new_resource.database_template_cookbook_name
  end
  template "#{new_resource.path}/shared/database.yml" do
    source database_template
    cookbook cookbook_name
    owner new_resource.owner
    group new_resource.group
    mode "644"
    variables(
        :host => host,
        :database => new_resource.database,
        :rails_env => new_resource.environment_name
    )
  end
end

def ensure_log_directory
  resource = new_resource

  log_directory = "#{resource.path}/shared/#{relative_log_path}"
  directory log_directory do
    owner resource.owner
    group resource.group
    recursive true
    mode '770'
  end
  log_directory
end

def symlink_logs
  log_directory = ensure_log_directory

  resource = new_resource
  log_file_path = "#{log_directory}/#{resource.environment_name}.log"

  logrotate_app resource.name do
    cookbook "logrotate"
    path log_file_path
    frequency "daily"
    rotate 30
    create "770 #{resource.owner} #{resource.group}"
  end
end
