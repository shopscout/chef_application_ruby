include ApplicationCookbook::ResourceBase

attribute :app_relative_path, :kind_of => String, :default => nil
attribute :owner, :kind_of => [String, NilClass], :default => nil
attribute :group, :kind_of => [String, NilClass], :default => nil
attribute :grunt_command, :kind_of => [String, NilClass], :default => nil
attribute :bower, :kind_of => [NilClass, TrueClass, FalseClass], :default => nil
attribute :bower_components_relative_path, :kind_of => String, :default => nil